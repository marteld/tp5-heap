package tas;

public class PriorityQ<T> {
    //implémentation d'une priorit queue dans laquelle les doublons (au sens de equals du T) sont autorisés

    Heap<ElemWithPriority<T>> h;

    public PriorityQ(boolean isMaxPQ) {
        h = new Heap<ElemWithPriority<T>>(isMaxPQ);
    }

    public int size(){
        return h.size();
    }

    public void add(T e, double p) {
        ElemWithPriority<T> elem = new ElemWithPriority<T>(e, p);
        h.add(elem);
    }

    public T getTop() {
        return h.getTop().getElem();
    }

    public T removeTop() {
        return h.removeTop().getElem();
    }


}
